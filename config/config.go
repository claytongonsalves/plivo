package config

import (
	"database/sql"
	"log"

	. "plivo/api"
	. "plivo/shared"
)

//Configuration structs holds all our components of the app
type Configuration struct {
	Db        ContactStore
	AppLogger AppLogger
	AppKeys   AppKeys
}

// NewConfiguration injects the config struct with our instances of logger, db etc etc
func NewConfiguration(db *sql.DB, logger *log.Logger, keys AppKeys) *Configuration {

	ContactDb := &ContactDataBase{Db: db}
	appLog := AppLogger{Logger: logger}
	appKeys := keys
	Conf := Configuration{Db: ContactDb, AppLogger: appLog, AppKeys: appKeys}
	return &Conf
}
