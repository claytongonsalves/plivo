package shared

//Routes
const (
	CONTACTS_ROUTE       = "/contacts"
	CONTACT_ROUTE        = "/contacts/{emailid}"
	CONTACT_SEARCH_ROUTE = "/contacts/search/"
	LOGIN                = "/login"

	//Queries

	GET_ALL_CONTACT_QUERY   = "SELECT * FROM contacts"
	GET_CONTACT_BY_ID_QUERY = "SELECT * FROM contacts WHERE EmailId = $1"
	CREATE_CONTACT_QUERY    = "INSERT INTO contacts(EmailId, Name, Phone, Address) VALUES($1, $2, $3, $4) returning EmailId"
	MODIFY_CONTACT_QUERY    = "UPDATE contacts SET EmailId=$1, Name = $2, Address= $3, Phone= $4 WHERE EmailId = $5"
	SEARCH_QUERY_NAME       = "SELECT * FROM contacts WHERE Name LIKE '%' || $1 || '%' and id > $2 ORDER BY Id LIMIT $3"
	SEARCH_QUERY            = "SELECT * FROM contacts WHERE %s LIKE '%' || $1 || '%' and id > $2 ORDER BY Id LIMIT $3"
	DELETE_QUERY            = "DELETE FROM contacts * WHERE EmailId = $1"

	//Messages

	INTERNAL_SERVER_ERROR       = "Internal Server Error"
	INCORRECT_USERNAME_PASSWORD = "Incorrect username or password"
	INVALID_TOKEN               = "Unauthorized. Invalid token"
	ID_ALREADY_PRESENT          = "EmailID already present"
	ID_DOES_NOT_EXIST           = "EmailId does not exist"
	BAD_REQUEST                 = "Invalid Input"

	CANNOT_OPEN_LOG_FILE      = "Cannot open log file."
	SUCCESS_MESSAGE           = "Server Up. Listening.... \n"
	PRIVATE_KEY_FILE_ERROR    = "Error reading private key from file"
	PRIVATE_KEY_ERROR         = "Error parsing private key"
	PUBLIC_KEY_FILE_ERROR     = "Error reading public key from file"
	PUBLIC_KEY_ERROR          = "Error parsing public key"
	DATABASE_CONNECTION_ERROR = "Not able to connect database."

	//Security Keys

	PathToPrivateKey = "./keys/app.rsa"
	PathToPublicKey  = "./keys/app.rsa.pub"

	//Files
	LOG_FILE = "ContactsApp.log"
)
