package shared

type Contact struct {
	Id      int
	EmailId string
	Name    string
	Phone   string
	Address string
}

type Token struct {
	Token string
}
