# Plivo take home Test


###ddd Prerequisites

Docker <br />
Docker-compose 

### Running the project

to run the project, navigate into plivo and run the following command
```
sudo docker-compose up --build
```
The project will now be running and listening to requests on port 8080


#### API definitions and Usage

| Name   | Method      | URL                                                                    | Protected |   
| ---    | ---         | ---                                                                    | ---       |
| Login  | `GET`       | `/login`                                                               | ✘         |
| List   | `GET`       | `/contacts`                                                            | ✘         |
| Create | `POST`      | `/contacts`                                                            | ✓         |
| Get    | `GET`       | `/contacts/{emailid}`                                                  | ✘         |
| Update | `PUT`       | `/contacts/{emailid}`                                                  | ✓         |
| Delete | `DELETE`    | `/contacts/{emailid}`                                                  | ✓         |
| Search | `GET`       | `/contacts/search?query="name"&searcby="name|emailid"&limit=10&prev=2` | ✘         |

Examples: <br />

1. Login : Login to get authorization token for accessing protected endpoints <br />
           Use admin/admin as username/password for the "Basic Authorization" method  <br />
           URL: `/login`<br />
           Method Type: `GET` <br />
           Headers: `authorization: Basic YWRtaW46YWRtaW4=`<br />
           Sample Response: `{"Token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJFeHBpcmVzQXQiOjIwMDAwLCJ1c2VybmFtZSI6ImFkbWluIn0"}`<br />

2. List all Contacts:   <br />
           URL: `/contacts`<br />
           Method Type: `GET` <br />
           Headers: None <br />
           <br />

3. List a contact based on contact ID:   <br />
           URL: `/contacts/{emailid}`<br />
           Method Type: `GET` <br />
           Headers: None <br />
        <br />
          

4. Create a Contact:   <br />
           URL: `/contacts`<br />
           Method Type: `POST` <br />
           Headers: `Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJFeHBpcmVzQXQiOjIwMDAwLCJ1c2VybmFtZSI6ImFkbWluIn0`<br />
           Request Body: 
          `{"EmailId":"subhash@abc.com","Name":"subhash",Phone:"1234567812",Address:"23 street, BLR"}`<br />
           <br />

5. Modify a Contact based on contact ID:   <br />
           URL: `/contacts/{emailid}`<br />
           Method Type: `PUT` <br />
           Headers: `Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJFeHBpcmVzQXQiOjIwMDAwLCJ1c2VybmFtZSI6ImFkbWluIn0` <br />
           Request Body: 
            `{"Name":"subhash",Phone:"1234567812",Address:"23 street, BLR"}`<br />
          <br />

6. Delete a Contact based on contact ID:   <br />
           URL: `/contacts/{emailid}`<br />
           Method Type: `DELETE` <br />
           Headers: `Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJFeHBpcmVzQXQiOjIwMDAwLCJ1c2VybmFtZSI6ImFkbWluIn0` <br />
        <br />

           
7. Search a contact based on keyword, the api will return contacts which contain the queried keyword:   <br />
           URL: `search?query="name"&searcby="name|emailid"&limit=10&prev=2`<br />
           Info: query is the search string, searchby can be set to emailid or name. limit defaults to 10. prev is to pass last recived value for pagination <br />
           Method Type: `GET` <br />
           Headers: None<br />
           Sample Response: `[{"EmailId":"subhash@abc.com","Name":"subhash",Phone:"1234567812",Address:"23 street, BLR"}]`  <br />          

##### Running the tests

```
ginkgo -r
```
##### Comments


* I chose to implement the solution in such a way that in the future it is easy to add/modify parts of the project without modifying too much of it. 

* I have used logging only in the handler layer. Doesn’t make sense to log on all layers and pollute the log file with unnecessary log messages 	
	
##### Future Improvements / limitations 



* The validation of the requests can be improved upon 

* Error handling and error messages can be made more robust. 

* Higher test case coverage,  tests are only covering the happy path and not any error cases

* Currently this ReST API is only capable of serving Json 

* Implementation of a cache to improve performance. 

* docker image is built for dev in mind

* handlers have too much responsibility. Need to use middleware to abstract common behaviour like authentication.

* configuration should come from environment variables to avoid leaking secrets 

