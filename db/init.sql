CREATE TABLE contacts (
		Id 			SERIAL integer,
	    EmailId     PRIMARY KEY,
	    Name        varchar(40) NOT NULL,
	    Phone       integer NOT NULL,
	    Address     integer NOT NULL
	);

	CREATE INDEX index_on_id ON contacts USING btree (Id);
	