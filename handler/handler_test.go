package handler_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "plivo/config"
	. "plivo/handler"
	. "plivo/shared"
)

func openFile(name string) (*os.File, error) {
	return os.OpenFile(name, os.O_CREATE|os.O_WRONLY, 0666)
}

func setupLogger() *log.Logger {
	// create file if not present
	file, err := os.OpenFile("ContactsAppTest.log", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal("Cannot open log file.")
		os.Exit(1)
	}
	Logger := log.New(file,
		"LOG: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	return Logger
}

func setupMockKeys() AppKeys {

	privateKeyBytes, err := ioutil.ReadFile("../keys/app.rsa")
	if err != nil {
		log.Fatal(PRIVATE_KEY_FILE_ERROR)
	}
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privateKeyBytes)
	if err != nil {
		log.Fatal(PRIVATE_KEY_ERROR)
	}
	publicKeyBytes, err := ioutil.ReadFile("../keys/app.rsa.pub")
	if err != nil {
		log.Fatalf(PUBLIC_KEY_FILE_ERROR)
	}
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(publicKeyBytes)
	if err != nil {
		log.Fatalf(PUBLIC_KEY_ERROR)
	}

	return AppKeys{
		PrivateKey: privateKey,
		PublicKey:  publicKey,
	}
}

var searchOutput = []Contact{
	{
		Id:      1,
		EmailId: "clayton@abc.com",
		Name:    "clayton",
		Phone:   "1234567890",
		Address: "23 street, BLR",
	},
}

var contactsOutput = []Contact{
	{
		Id:      1,
		EmailId: "clayton@abc.com",
		Name:    "clayton",
		Phone:   "1234567890",
		Address: "23 street, BLR",
	},
	{
		Id:      2,
		EmailId: "subhash@abc.com",
		Name:    "subhash",
		Phone:   "1234567812",
		Address: "12 street, BLR",
	},
}

var contact = Contact{

	Id:      2,
	EmailId: "subhash@abc.com",
	Name:    "subhash",
	Phone:   "1234567812",
	Address: "12 street, BLR",
}

var contactString = []byte(`{
	Id: 2,
		EmailId:    "subhash@abc.com",
		Name:       "subhash",
		Phone:      "1234567812",
		Address:     "12 street, BLR",
}`)

var updatedContactString = []byte(`{
	Id: 2,
		EmailId:    "subhash@abc.com",
		Name:       "subhash",
		Phone:      "1234567812",
		Address:     "23 street, BLR",
}`)

var mockKeys = setupMockKeys()

var logger = AppLogger{Logger: setupLogger()}

type mockDB struct{}

var mockdb = &mockDB{}

var keys = AppKeys{
	PublicKey:  mockKeys.PublicKey,
	PrivateKey: mockKeys.PrivateKey,
}

var conf = &Configuration{mockdb, logger, keys}

var handler = &ContactHandler{Conf: conf}

func getToken() string {
	response := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/login", nil)
	req.SetBasicAuth("admin", "admin")
	http.HandlerFunc(handler.LoginHandler).ServeHTTP(response, req)
	var token Token
	json.Unmarshal(response.Body.Bytes(), &token)

	return token.Token
}

func (mockdb *mockDB) GetAllContacts() ([]Contact, error) {
	return contactsOutput, nil
}

func (mockdb *mockDB) GetContactById(emailid string) (Contact, error) {
	for _, contact := range contactsOutput {

		if contact.EmailId == emailid {
			return contact, nil
		}
	}
	return Contact{}, errors.New("contact not found")
}

func (mockdb *mockDB) CreateContact(contact Contact) error {

	newcontact := Contact{
		Id:      3,
		EmailId: contact.EmailId,
		Name:    contact.Name,
		Address: contact.Address,
		Phone:   contact.Phone,
	}

	contactsOutput = append(contactsOutput, newcontact)
	return nil
}

func (mockdb *mockDB) DeleteContactById(emailid string) error {
	for index, contact := range contactsOutput {

		if contact.EmailId == emailid {
			contactsOutput = append(contactsOutput[:index], contactsOutput[index+1:]...)
			return nil
		}
	}
	return errors.New("contact not found")
}

func (mockdb *mockDB) ModifyContact(contact Contact) error {

	for _, contactValue := range contactsOutput {
		if contactValue.EmailId == contact.EmailId {
			contactValue.Name = contact.Name
			contactValue.Address = contact.Address
			contactValue.Phone = contact.Phone
			return nil
		}
	}
	return errors.New("contact not found")
}

func (mockdb *mockDB) Search(query, searchby string, prev, limit int) ([]Contact, error) {
	var searchedContacts []Contact
	for _, contact := range contactsOutput {
		if strings.Contains(contact.Name, query) {
			searchedContacts = append(searchedContacts, contact)
		}
		fmt.Print(searchedContacts)
		return searchedContacts, nil
	}

	return nil, errors.New("contact not found")
}

var _ = Describe("Http Handler", func() {

	Context("Get all contacts :: GET :: /contacts ", func() {

		It("returns a list of all the contacts", func() {

			response := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/contacts", nil)
			http.HandlerFunc(handler.ContactsHandler).ServeHTTP(response, req)

			var contacts []Contact
			json.Unmarshal(response.Body.Bytes(), &contacts)

			Expect(contacts).To(Equal(contactsOutput))
			Expect(response.Code).To(Equal(http.StatusOK))
		})
	})

	Context("Get a Contact by contact ID :: GET :: /contacts/{id}", func() {

		It("returns a contact based on the contact ID Given in the path param", func() {

			router := mux.NewRouter()
			router.HandleFunc(CONTACT_ROUTE, handler.ContactHandler)

			response := httptest.NewRecorder()

			req, _ := http.NewRequest("GET", "/contacts/subhash@abc.com", nil)

			router.ServeHTTP(response, req)

			var contact Contact
			json.Unmarshal(response.Body.Bytes(), &contact)

			Expect(contact).To(Equal(contactsOutput[1]))
			Expect(response.Code).To(Equal(http.StatusOK))
		})
	})

	Context("Create Contact :: POST :: /contacts ", func() {

		It("creates a contact based on the given request body", func() {

			router := mux.NewRouter()
			router.HandleFunc(CONTACTS_ROUTE, handler.ContactHandler)

			payload := []byte(`
				{
				EmailId:    "subhash@abc.com",
				Name:       "subhash",
				Phone:      "1234567812",
				Address:     "23 street, BLR",
				}
				`)
			req, _ := http.NewRequest("POST", "/contacts", bytes.NewBuffer(payload))
			req.Header.Set("Content-Type", "application/json")

			token := getToken()

			req.Header.Set("authorization", "bearer "+token)

			response := httptest.NewRecorder()

			router.ServeHTTP(response, req)
			fmt.Println(response)
			Expect(response.Code).To(Equal(http.StatusOK))
		})
	})

	Context("Update Contact :: PUT :: /contacts/{emailid} ", func() {

		It("update contact based on path param and request body", func() {

			router := mux.NewRouter()
			router.HandleFunc(CONTACT_ROUTE, handler.ContactHandler)

			payload := []byte(`
				{
				Name:       "subhash",
				Phone:      "1234234812",
				Address:     "23 street, BLR",
				}
				`)
			req, _ := http.NewRequest("PUT", "/contacts/subhash@abc.com", bytes.NewBuffer(payload))
			req.Header.Set("Content-Type", "application/json")

			token := getToken()
			req.Header.Set("authorization", "bearer "+token)

			response := httptest.NewRecorder()

			router.ServeHTTP(response, req)

			Expect(response.Code).To(Equal(http.StatusOK))
		})
	})

	Context("Delete Contact :: DELETE :: /contacts/2 ", func() {

		It("deletes a contact based on the given id in path param", func() {

			router := mux.NewRouter()
			router.HandleFunc(CONTACT_ROUTE, handler.ContactHandler)

			req, _ := http.NewRequest("DELETE", "/contacts/1", nil)
			req.Header.Set("Content-Type", "application/json")

			token := getToken()
			req.Header.Set("authorization", "bearer "+token)

			response := httptest.NewRecorder()

			router.ServeHTTP(response, req)

			Expect(response.Code).To(Equal(http.StatusOK))
		})
	})

	Context("Search Contact :: GET :: /contacts ", func() {

		It("searches for a contact based on the param", func() {

			response := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "contacts/search/?query=clayton&searchby=name", nil)
			http.HandlerFunc(handler.SearchHandler).ServeHTTP(response, req)

			var contacts []Contact
			json.Unmarshal(response.Body.Bytes(), &contacts)

			Expect(contacts).To(Equal(searchOutput))
			Expect(response.Code).To(Equal(http.StatusOK))
		})
	})

})
