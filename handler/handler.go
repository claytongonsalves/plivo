package handler

import (
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"

	. "plivo/config"
	. "plivo/shared"
)

type ContactHandler struct {
	Conf *Configuration
}

func newContactHandler(conf *Configuration) *ContactHandler {
	return &ContactHandler{Conf: conf}
}

func RegisterHandlers(router *mux.Router, conf *Configuration) {

	rh := newContactHandler(conf)

	router.HandleFunc(LOGIN, rh.LoginHandler).Methods("GET")
	router.HandleFunc(CONTACTS_ROUTE, rh.ContactsHandler).Methods("GET", "POST")
	router.HandleFunc(CONTACT_ROUTE, rh.ContactHandler).Methods("GET", "DELETE", "PUT")
	router.HandleFunc(CONTACT_SEARCH_ROUTE, rh.SearchHandler).Methods("GET")

}

func (rh *ContactHandler) LoginHandler(res http.ResponseWriter, req *http.Request) {

	conf := rh.Conf
	log := conf.AppLogger

	log.Logger.Printf("\n Route :%s", LOGIN)

	//validate user credentials by the credentails passed by Basic Auth

	username, password, _ := req.BasicAuth()

	if username != "admin" && password != "admin" {
		log.Logger.Println(INCORRECT_USERNAME_PASSWORD)
		processError(res, errors.New(INCORRECT_USERNAME_PASSWORD))
		return
	}

	//creating a claim

	claims := jwt.MapClaims{
		"username":  username,
		"ExpiresAt": 20000,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	tokenString, err := token.SignedString(conf.AppKeys.PrivateKey)

	if err != nil {
		log.Logger.Println("Failed to create Token")
		processError(res, errors.New(INTERNAL_SERVER_ERROR))
		return
	}

	processJson(res, http.StatusOK, map[string]string{"Token": tokenString})

}

func (rh *ContactHandler) ContactsHandler(res http.ResponseWriter, req *http.Request) {

	conf := rh.Conf
	log := conf.AppLogger

	switch req.Method {

	case "GET":

		log.Logger.Printf("\n GET::Route : %s", CONTACTS_ROUTE)
		contacts, err := conf.Db.GetAllContacts()

		if err != nil {
			log.Logger.Println("error::", err)
			processError(res, err)
			return
		}

		processJson(res, http.StatusOK, contacts)

	case "POST":

		log.Logger.Printf("\n POST::Route : %s", CONTACTS_ROUTE)

		if !isTokenValid(req, conf.AppKeys.PublicKey) {
			log.Logger.Println(INVALID_TOKEN)
			processError(res, errors.New(INVALID_TOKEN))
			return
		}

		contactRequest, err := ValidateAndDecodeContactRequest(req)
		if !req.Close {
			log.Logger.Println("error::", "unable to close request")
		}

		if err != nil {
			log.Logger.Println("error::", BAD_REQUEST)
			processError(res, errors.New(BAD_REQUEST))
			return
		}

		err = conf.Db.CreateContact(contactRequest)

		if err != nil {
			log.Logger.Println("error::", err)
			processError(res, err)
			return
		}

		processJson(res, http.StatusCreated, map[string]string{"Result": "Successfully created"})

	}
}

func (rh *ContactHandler) ContactHandler(res http.ResponseWriter, req *http.Request) {

	conf := rh.Conf
	log := conf.AppLogger

	pathParam := mux.Vars(req)
	emailId := pathParam["emailid"]

	switch req.Method {

	case "GET":

		log.Logger.Printf("\n GET::Route : %s :: Pathparam::%s", CONTACT_ROUTE, emailId)

		contact, err := conf.Db.GetContactById(emailId)
		if err != nil {
			log.Logger.Println("error::", err)
			processError(res, err)
			return
		}
		processJson(res, http.StatusOK, contact)

	case "DELETE":

		log.Logger.Printf("\n DELETE::Route : %s :: Pathparam::%s", CONTACT_ROUTE, emailId)

		if !isTokenValid(req, conf.AppKeys.PublicKey) {
			log.Logger.Println("error::", INVALID_TOKEN)
			processError(res, errors.New(INVALID_TOKEN))
			return
		}
		if !isContactPresent(emailId, conf) {
			log.Logger.Println("error::", ID_DOES_NOT_EXIST)
			processError(res, errors.New(ID_DOES_NOT_EXIST))
			return
		}

		err := conf.Db.DeleteContactById(emailId)

		if err != nil {
			log.Logger.Println("error::", err)
			processError(res, err)
			return
		}

		processJson(res, http.StatusOK, map[string]string{"result": "Contact successfully deleted"})

	case "PUT":

		log.Logger.Printf("\n PUT::Route : %s :: Pathparam::%s", CONTACT_ROUTE, emailId)

		if !isTokenValid(req, conf.AppKeys.PublicKey) {
			log.Logger.Println("error::", INVALID_TOKEN)
			processError(res, errors.New(INVALID_TOKEN))
			return
		}

		contactRequest, err := ValidateAndDecodeContactRequest(req)
		contactRequest.EmailId = emailId

		if err != nil {
			log.Logger.Println("error::", BAD_REQUEST)
			processError(res, errors.New(BAD_REQUEST))
			return
		}
		if !isContactPresent(emailId, conf) {
			log.Logger.Println("error::", ID_DOES_NOT_EXIST)
			processError(res, errors.New(ID_DOES_NOT_EXIST))
			return
		}

		err = conf.Db.ModifyContact(contactRequest)
		if err != nil {
			log.Logger.Println("error::", err)
			processError(res, err)
			return
		}

		processJson(res, http.StatusOK, map[string]string{"result": "Contact successfully modfied"})

	}
}

func (rh *ContactHandler) SearchHandler(res http.ResponseWriter, req *http.Request) {

	conf := rh.Conf
	log := conf.AppLogger

	query := req.FormValue("query")
	searchby := req.FormValue("searchby")
	limit, err := strconv.Atoi(req.FormValue("limit"))
	prev, err := strconv.Atoi(req.FormValue("prev"))

	if err != nil {
		log.Logger.Println("error::", err)
		processError(res, err)
		return
	}

	log.Logger.Printf("\n GET::Route : %s :: Queryparam::%s", CONTACT_SEARCH_ROUTE, query)

	if limit == 0 {
		limit = 10
	}

	contacts, err := conf.Db.Search(query, searchby, limit, prev)

	if err != nil {
		log.Logger.Println("error::", err)
		processError(res, err)
		return
	}

	processJson(res, http.StatusOK, contacts)

}
