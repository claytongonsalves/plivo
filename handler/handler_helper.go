package handler

import (
	"crypto/rsa"
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/pkg/errors"

	. "plivo/config"
	. "plivo/shared"
)

func processJson(res http.ResponseWriter, httpCode int, data interface{}) {

	response, _ := json.Marshal(data)
	res.WriteHeader(httpCode)
	res.Write(response)
}

func processError(res http.ResponseWriter, err error) {

	errString := err.Error()

	switch errString {

	case "Internal Server Error: sql: no rows in result set":
		processJson(res, http.StatusNotFound, map[string]string{"result": "Not Found"})

	case INCORRECT_USERNAME_PASSWORD:
		processJson(res, http.StatusUnauthorized, map[string]string{"result": INCORRECT_USERNAME_PASSWORD})

	case INVALID_TOKEN:
		processJson(res, http.StatusUnauthorized, map[string]string{"result": INVALID_TOKEN})

	case ID_ALREADY_PRESENT:
		processJson(res, http.StatusBadRequest, map[string]string{"result": ID_ALREADY_PRESENT})

	case ID_DOES_NOT_EXIST:
		processJson(res, http.StatusNotFound, map[string]string{"result": ID_DOES_NOT_EXIST})

	case BAD_REQUEST:
		processJson(res, http.StatusNotFound, map[string]string{"result": BAD_REQUEST})

	default:
		processJson(res, http.StatusInternalServerError, map[string]string{"result": INTERNAL_SERVER_ERROR})
	}

}

func isContactPresent(id string, conf *Configuration) bool {

	_, err := conf.Db.GetContactById(id)

	if err != nil {
		return false
	}

	return true
}

func isTokenValid(req *http.Request, publicKey *rsa.PublicKey) bool {

	token, _ := request.ParseFromRequest(req, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if token != nil {
		if token.Valid {
			return true
		}
	}
	return false
}

func ValidateAndDecodeContactRequest(req *http.Request) (Contact, error) {

	var contact Contact

	err := json.NewDecoder(req.Body).Decode(&contact)
	if err != nil {
		return Contact{}, errors.New("Invalid request body")
	}

	if contact.Name == "" {
		return Contact{}, errors.New("Name Cannot be empty")
	} else if contact.Address == "" {
		return Contact{}, errors.New("Address Cannot be empty")
	} else if contact.Phone == "" {
		return Contact{}, errors.New("Phone Cannot be empty")
	}
	return contact, nil
}
