package api

import (
	"database/sql"
	"fmt"

	"github.com/pkg/errors"

	. "plivo/shared"
)

type ContactStore interface {
	GetAllContacts() ([]Contact, error)
	GetContactById(id string) (Contact, error)
	CreateContact(contact Contact) error
	ModifyContact(contact Contact) error
	Search(query, searchby string, limit, prev int) ([]Contact, error)
	DeleteContactById(id string) error
}

type ContactDataBase struct {
	Db *sql.DB
}

func (Dbase *ContactDataBase) GetAllContacts() ([]Contact, error) {

	Db := Dbase.Db

	rows, err := Db.Query(GET_ALL_CONTACT_QUERY)

	if err != nil {
		return nil, errors.New(INTERNAL_SERVER_ERROR)
	}
	defer rows.Close()

	contacts := make([]Contact, 0)

	for rows.Next() {
		var contact Contact
		err := rows.Scan(&contact.Id, &contact.EmailId, &contact.Name, &contact.Phone, &contact.Address)
		if err != nil {
			return nil, errors.New(INTERNAL_SERVER_ERROR)
		}

		contacts = append(contacts, contact)
	}

	return contacts, nil
}

func (Dbase *ContactDataBase) GetContactById(emailid string) (Contact, error) {

	Db := Dbase.Db

	row := Db.QueryRow(GET_CONTACT_BY_ID_QUERY, emailid)

	var contact Contact
	err := row.Scan(&contact.Id, &contact.EmailId, &contact.Name, &contact.Phone, &contact.Address)
	if err != nil {
		return Contact{}, errors.New(INTERNAL_SERVER_ERROR)
	}

	return contact, nil
}

func (Dbase *ContactDataBase) CreateContact(contact Contact) error {

	Db := Dbase.Db

	row := Db.QueryRow(CREATE_CONTACT_QUERY, contact.EmailId, contact.Name, contact.Phone, contact.Address)

	var emailId int
	row.Scan(&emailId)

	return nil
}

func (Dbase *ContactDataBase) ModifyContact(contact Contact) error {

	Db := Dbase.Db

	_, err := Db.Exec(MODIFY_CONTACT_QUERY, contact.EmailId, contact.Name, contact.Address, contact.Phone, contact.EmailId)

	if err != nil {
		return errors.New(INTERNAL_SERVER_ERROR)
	}

	return nil
}

func (Dbase *ContactDataBase) Search(query, searchby string, limit, prev int) ([]Contact, error) {

	Db := Dbase.Db
	var queryString string

	if searchby == "name" {
		queryString = fmt.Sprintf(SEARCH_QUERY, "Name")
	} else {
		queryString = fmt.Sprintf(SEARCH_QUERY, "emailId")
	}
	rows, err := Db.Query(queryString, query, prev, limit)

	if err != nil {
		return []Contact{}, errors.New(INTERNAL_SERVER_ERROR)
	}
	defer rows.Close()

	contacts := make([]Contact, 0)
	for rows.Next() {

		var contact Contact
		err := rows.Scan(&contact.Id, &contact.EmailId, &contact.Name, &contact.Phone, &contact.Address)

		if err != nil {
			return nil, errors.New(INTERNAL_SERVER_ERROR)
		}

		contacts = append(contacts, contact)
	}

	if err = rows.Err(); err != nil {
		return nil, errors.New(INTERNAL_SERVER_ERROR)
	}

	return contacts, nil

}

func (Dbase *ContactDataBase) DeleteContactById(emailid string) error {

	Db := Dbase.Db

	_, err := Db.Query(DELETE_QUERY, emailid)

	if err != nil {
		return errors.New(INTERNAL_SERVER_ERROR)
	}

	return nil
}
